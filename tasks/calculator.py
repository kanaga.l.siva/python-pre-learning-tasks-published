def calculator(a, b, operator):
    # ==============
    if operator == "+":
    	return (bin(a+b)[2:])
    elif operator == "-":
    	return (bin(a-b)[2:])
    elif operator == "*":
    	return (bin(a*b)[2:])
    elif operator == "/":
    	return (bin(int(a/b))[2:])
    else:
    	print("Invalid Output")

    # ==============

print(calculator(2, 4, "+")) # Should print 6 to the console
print(calculator(10, 3, "-")) # Should print 7 to the console
print(calculator(4, 7, "*")) # Should print 28 to the console
print(calculator(100, 2, "/")) # Should print 50 to the console
